import { createApp } from 'vue'
import App from './App.vue'

import './index.css'

import VueWriter from "vue-writer"
import VueSmoothScroll from 'vue3-smooth-scroll'

import { routes } from './routes.js'
import { createRouter, createWebHistory } from 'vue-router'

const app = createApp(App)

const router = createRouter({
    history: createWebHistory(),
    routes,
  })

app.use(VueWriter)
app.use(VueSmoothScroll)
app.use(router)
app.mount('#app')
