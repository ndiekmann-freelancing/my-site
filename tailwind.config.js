module.exports = {
  content: [
    "./index.html",
    "./src/**/*.{vue,js,ts,jsx,tsx}",
  ],
  theme: {
    extend: {
      colors: {
        'my-purple': '#0C0A1D',
        'button-purple': '#5B3FD2',
      }
    },
  },
  plugins: [],
}